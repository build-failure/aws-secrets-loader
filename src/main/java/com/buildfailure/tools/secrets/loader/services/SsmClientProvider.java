package com.buildfailure.tools.secrets.loader.services;

import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.ssm.SsmClient;
import com.buildfailure.tools.secrets.loader.props.Secrets;

@Service
public class SsmClientProvider {

    private final Secrets secrets;

    public SsmClientProvider(Secrets secrets) {
        this.secrets = secrets;
    }

    public SsmClient get(){
        return SsmClient.builder()
            .region(secrets.getRegion())
            .build();
    }
}
