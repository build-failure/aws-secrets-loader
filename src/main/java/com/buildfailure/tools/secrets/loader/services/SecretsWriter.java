package com.buildfailure.tools.secrets.loader.services;

import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;

import static java.text.MessageFormat.format;

@Service
public class SecretsWriter {

    public void write(String name, String value){
        try (FileWriter fileWriter = new FileWriter(name)) {
            fileWriter.write(value);
        } catch (IOException e){
            throw new RuntimeException(format("Can not write to file {0}", name), e);
        }
    }
}
