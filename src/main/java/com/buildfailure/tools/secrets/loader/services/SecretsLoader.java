package com.buildfailure.tools.secrets.loader.services;

import com.buildfailure.tools.secrets.loader.props.Secret;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecretsLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecretsLoader.class);

    private final SecretsReader secretsReader;

    private final SecretsWriter secretsWriter;

    public SecretsLoader(
        SecretsReader secretsReader,
        SecretsWriter secretsWriter
    ) {
        this.secretsReader = secretsReader;
        this.secretsWriter = secretsWriter;
    }

    public void load(List<Secret> items){
        LOGGER.info("Loading {} secret(s).", items.size());
        items.forEach(this::load);
    }

    private void load(Secret item){
        LOGGER.info("Reading secret '{}' into '{}'.", item.getSource(), item.getDestination());

        String value = secretsReader.read(item.getSource());

        secretsWriter.write(item.getDestination(), value);
    }
}
