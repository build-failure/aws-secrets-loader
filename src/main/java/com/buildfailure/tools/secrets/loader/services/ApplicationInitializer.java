package com.buildfailure.tools.secrets.loader.services;

import org.springframework.stereotype.Service;
import com.buildfailure.tools.secrets.loader.props.Secrets;

import javax.annotation.PostConstruct;

@Service
public class ApplicationInitializer {

    private final Secrets secrets;

    private final SecretsLoader secretsLoader;

    public ApplicationInitializer(Secrets secrets, SecretsLoader secretsLoader) {
        this.secrets = secrets;
        this.secretsLoader = secretsLoader;
    }

    @PostConstruct
    public void init(){
        secretsLoader.load(secrets.getItems());
    }
}
