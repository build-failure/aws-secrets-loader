package com.buildfailure.tools.secrets.loader.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.regions.Region;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "secrets-loader")
public class Secrets {

    private Region region = Region.EU_WEST_1;

    private List<Secret> items;

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<Secret> getItems() {
        return items;
    }

    public void setItems(List<Secret> items) {
        this.items = items;
    }
}
