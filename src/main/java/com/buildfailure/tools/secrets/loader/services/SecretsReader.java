package com.buildfailure.tools.secrets.loader.services;

import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;

@Service
public class SecretsReader {

    private final SsmClientProvider ssmClientProvider;

    public SecretsReader(SsmClientProvider ssmClientProvider) {
        this.ssmClientProvider = ssmClientProvider;
    }

    public String read(String source){
        GetParameterResponse response = getParameter(source);

        return response.parameter().value();
    }

    private GetParameterResponse getParameter(String name) {
        GetParameterRequest request = GetParameterRequest.builder()
            .name(name)
            .withDecryption(true)
            .build();
        return ssmClientProvider.get().getParameter(request);
    }
}
