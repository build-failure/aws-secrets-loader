package com.buildfailure.tools.secrets.loader.services;

import com.buildfailure.tools.secrets.loader.props.Secrets;
import org.junit.Test;
import software.amazon.awssdk.regions.Region;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SsmClientProviderTest {

    private final Secrets secrets = mock(Secrets.class);

    private final SsmClientProvider ssmClientProvider = new SsmClientProvider(secrets);

    @Test
    public void shouldGetSsmClient(){
        when(secrets.getRegion()).thenReturn(Region.EU_WEST_1);

        ssmClientProvider.get();
    }
}
