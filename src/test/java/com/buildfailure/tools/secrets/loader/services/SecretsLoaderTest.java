package com.buildfailure.tools.secrets.loader.services;

import com.buildfailure.tools.secrets.loader.props.Secret;
import org.junit.Test;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

public class SecretsLoaderTest {

    private static final String SECRET_SOURCE = "foo";

    private static final String SECRET_DESTINATION = "bar";

    private static final String SECRET_VALUE = "baz";

    private final SecretsReader secretsReader = mock(SecretsReader.class);

    private final SecretsWriter secretsWriter = mock(SecretsWriter.class);

    private final SecretsLoader secretsLoader = new SecretsLoader(secretsReader, secretsWriter);

    private final Secret secret = mock(Secret.class);

    @Test
    public void shouldLoadSecrets(){
        when(secret.getSource()).thenReturn(SECRET_SOURCE);
        when(secret.getDestination()).thenReturn(SECRET_DESTINATION);
        when(secretsReader.read(SECRET_SOURCE)).thenReturn(SECRET_VALUE);

        secretsLoader.load(singletonList(secret));

        verify(secretsWriter).write(SECRET_DESTINATION, SECRET_VALUE);
    }

}
