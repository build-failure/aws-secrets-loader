package com.buildfailure.tools.secrets.loader.props;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.buildfailure.tools.secrets.loader.services.ApplicationInitializer;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SecretsTest {

    @MockBean
    private ApplicationInitializer applicationInitializer;

    @Autowired
    private Secrets secrets;

    @Test
    public void shouldReturnSecrets(){
        assertEquals(secrets.getItems().size(), 1);
        Secret secret = secrets.getItems().get(0);
        assertEquals(secret.getSource(), "foo");
        assertEquals(secret.getDestination(), "bar");
    }
}
