package com.buildfailure.tools.secrets.loader.services;

import org.junit.Test;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;
import software.amazon.awssdk.services.ssm.model.Parameter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SecretsReaderTest {

    private static final String PARAMETER_NAME = "foo";

    private static final String PARAMETER_VALUE = "bar";

    private final SsmClientProvider ssmClientProvider = mock(SsmClientProvider.class);

    private final SsmClient ssmClient = mock(SsmClient.class);

    private final SecretsReader secretsReader = new SecretsReader(ssmClientProvider);

    @Test
    public void shouldReadDecryptedParameterValue(){
        when(ssmClientProvider.get()).thenReturn(ssmClient);
        when(ssmClient.getParameter(GetParameterRequest.builder()
            .name(PARAMETER_NAME)
            .withDecryption(true)
            .build()))
            .thenReturn(
                GetParameterResponse.builder()
                    .parameter(
                        Parameter.builder().value(PARAMETER_VALUE).build())
                    .build()
            );

        String value = secretsReader.read(PARAMETER_NAME);

        assertEquals(value, PARAMETER_VALUE);
    }
}
