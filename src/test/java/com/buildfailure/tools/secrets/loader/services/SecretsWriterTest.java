package com.buildfailure.tools.secrets.loader.services;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

public class SecretsWriterTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private static final String SECRET_NAME = "foo";

    private static final String SECRET_VALUE = "bar";

    private final SecretsWriter secretsWriter = new SecretsWriter();

    @Test
    public void shouldWriteSecret() throws IOException {
        String path = temporaryFolder.getRoot().getAbsolutePath();
        String filePath = path + "/" + SECRET_NAME;

        secretsWriter.write(filePath, SECRET_VALUE);

        String actualValue = FileUtils.readFileToString(Paths.get(filePath).toFile(), UTF_8);
        assertEquals(actualValue, SECRET_VALUE);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowError(){
        String path = temporaryFolder.getRoot().getAbsolutePath();

        secretsWriter.write(path, SECRET_VALUE);
    }

}


