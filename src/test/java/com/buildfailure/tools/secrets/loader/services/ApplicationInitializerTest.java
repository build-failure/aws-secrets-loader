package com.buildfailure.tools.secrets.loader.services;

import com.buildfailure.tools.secrets.loader.props.Secrets;
import org.junit.Test;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.*;

public class ApplicationInitializerTest {

    private final Secrets secrets = mock(Secrets.class);

    private final SecretsLoader secretsLoader = mock(SecretsLoader.class);

    private final ApplicationInitializer initializer = new ApplicationInitializer(secrets, secretsLoader);

    @Test
    public void shouldInit(){
        when(secrets.getItems()).thenReturn(emptyList());

        initializer.init();

        verify(secretsLoader).load(secrets.getItems());
    }
}
