[![pipeline status](https://gitlab.com/build-failure/aws-secrets-loader/badges/master/pipeline.svg)](https://gitlab.com/build-failure/aws-secrets-loader/-/commits/master)
[![coverage report](https://gitlab.com/build-failure/aws-secrets-loader/badges/master/coverage.svg)](https://gitlab.com/build-failure/aws-secrets-loader/-/jobs/artifacts/master/file/target/site/jacoco/index.html?job=build-artifact)
[![release](https://img.shields.io/badge/release-1.0.0-blue.svg)](https://gitlab.com/build-failure/aws-secrets-loader/container_registry/2109841)

# AWS Secrets Loader

Fetches secrets managed within [Systems Manager Parameter Store] to a file system.

Can be used to load certificate and key for SSL/TLS termination within a Elastic Container Service when
using a task as reverse proxy.

## Architecture

![architecture](./docs/img/architecture.png "Architecture Overview")


## Prerequisites

- Java >= 8
- Maven >= 3.6.x
- Docker >=17.x

## Config

| Parameter | Type | Default | Description |
| --- | --- | --- | --- |
| `SECRETS_LOADER_ITEMS` | Environment variable | `[]` | Lists secret items to be loaded. |
| `AWS_ACCESS_KEY_ID` | Environment variable | - | AWS access key ID [*] |
| `AWS_SECRET_ACCESS_KEY` | Environment variable | - | AWS secret access key [*] |
| `AWS_SESSION_TOKEN` | Environment variable | - | AWS session token [*] |

[*] required for local setup only, utilizes `ContainerCredentialsProvider` when running within ECS.

Check [options for setting environment variables](https://docs.docker.com/compose/environment-variables/) when working with Docker Compose.

### Load Secrets
Refer to the following example when configuring secrets to load.

Given is the certificate stored as parameter `mydomain-cert` and key stored as parameter `mydomain-key` within [Systems Manager Parameter Store] as secure strings.

Following configuration can be used to fetch and store secrets.

```yaml
SECRETS_LOADER_ITEMS_0_SOURCE: "mydomain-cert"
SECRETS_LOADER_ITEMS_0_DESTINATION: "/certs/mydomain.crt"
SECRETS_LOADER_ITEMS_1_SOURCE: "mydomain-key"
SECRETS_LOADER_ITEMS_1_DESTINATION: "/certs/mydomain.key"
```

## Setup

Development setup consists of the `proxy`(nginx:1.21.1) and the `secrets-loader` sharing a single volume `certs`.

The `secrets-loader` is responsible for providing `mydomain.crt` and `mydomain.key` on the shared volume for the `proxy`
to be able to serve traffic over HTTPS.

See the [docker-compose.yml](docker-compose.yml) for details.

## Run

```bash
docker-compose up -d
```

Access https://localhost:8443.

## Test
```bash
mvn clean test
```

## Build

```bash
mvn clean package && cd dev && docker-compose build
```

## CI/CD

s. [.gitlab-ci.yml](.gitlab-ci.yml)

## License

See the [LICENSE.md](LICENSE.md) file for details.

[Systems Manager Parameter Store]: https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html
